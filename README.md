# README #

This is the code for Lee et al. bioRxiv 2018 https://www.biorxiv.org/content/early/2018/12/26/503235

### What is this repository for? ###

This is the code for Lee et al. bioRxiv 2018 https://www.biorxiv.org/content/early/2018/12/26/503235
### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact